#!/usr/bin/env bash
rm -rf ./output
mkdir output
PKGDEST=$(pwd)/output
PKGARCH=$(uname -m)
sed -i -r "s|changepkgdest|${PKGDEST}|g;s|changearch|${PKGARCH}|g" makepkg-waitron/makepkg.conf
grep PKGDEST makepkg-waitron/makepkg.conf
cd aur_waitron
makepkg -cCs --noconfirm --config ../makepkg-waitron/makepkg.conf
